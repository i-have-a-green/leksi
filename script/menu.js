/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection
------------------
// 4 - Modif topbar background on scroll

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("topbar").classList.toggle("menu-open");
}

// 3 - Scroll detection
// #topbar gets sticky on scroll

var derniere_position_de_scroll_connue = 0;
var ticking = false;
var menu = document.getElementById("topbar");
var sticky = menu.offsetTop;

window.addEventListener('scroll', function(e) {
    if ( menu.classList.contains("menu-open") ) {
        // nothing happens
        //console.log('Menu Ouvert - pas de scroll');
    } else {
        if (!ticking) {
            //console.log('Détection du scroll');
            window.setTimeout(function() {
                ticking = false;
                if (window.pageYOffset > sticky) {
                    menu.classList.add("sticky");
                } else {
                    menu.classList.remove("sticky");
                }
            }, 50); //fréquence du scroll
        }
        ticking = true;
    }
});


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/

