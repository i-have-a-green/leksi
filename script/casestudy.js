var buttons_casestudy = document.getElementsByClassName('button_casestudy');
Array.prototype.forEach.call(buttons_casestudy, function(button_casestudy) {
    button_casestudy.addEventListener('click', function(e) {

        var archiveCasestudy = document.getElementsByClassName('archiveCasestudy');
        Array.prototype.forEach.call(archiveCasestudy, function(casestudy) {

            var classeCasestudy = casestudy.classList;
            //console.log((button_casestudy.dataset.slugCasestudy + " == " + casestudy.dataset.slugCasestudy));
            if (button_casestudy.dataset.slugCasestudy == casestudy.dataset.slugCasestudy || button_casestudy.dataset.slugCasestudy == "0") {
                classeCasestudy.remove("hide");
            } else {
                classeCasestudy.add("hide");
            }

        });
    });
});