<?php get_header('home'); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
        <?php the_content(); ?>
</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_template_part( 'template-parts/contactUs' );?>

<?php get_footer(); ?>