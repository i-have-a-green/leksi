<?php
/**
 * The template for displaying archive pages
 *
 */
get_header();
?>

<main>

<?php if ( have_posts() ) : ?>

	<header id="archive-header" class="wrapper">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>

		<!-- Titre-->
		<h1><?php single_term_title(); ?></h1>

		<!-- Category Menu-->
		<nav class="archive-nav">

			<a href="<?php the_permalink( get_field('archive_post','option') );?>" title="<?php _e('Tous les articles','leksi');?>" class="discrete-link letter-spacing h6-like">
				<?php _e('Tous','leksi');?>
			</a>

			<?php $terms = get_terms( 
				array(
					'taxonomy' => 'category',
					'hide_empty' => true,
				) 
			);

			foreach ($terms as $row):?>

				<a href="<?php echo get_term_link($row);?>" title="<?php _e('Voir les articles ','leksi');?><?php echo $row->name;?>" class="discrete-link letter-spacing h6-like">
					<?php echo '#'. $row->name;?>
				</a>

			<?php endforeach;?>

		</nav>
		
	</header>

	<section class="wrapper archive-listing">

		<div class="grid-post wrapper-medium">

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/archive', get_post_type() );
			endwhile; ?>

		</div>

		<nav class="pagination wrapper-medium">
			<?php joints_page_navi()?>
			<?php wp_reset_query(); ?>
		</nav>

		<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</section>

	<?php get_template_part( 'template-parts/newsletter' ); ?>
	<?php get_template_part( 'template-parts/contactUs' );?>

</main>

<?php get_footer(); ?>
