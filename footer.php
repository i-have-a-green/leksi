
</div><!-- div #content -->

<footer id="footer" class="white leksi-blue-bg wrapper">

	<div id="footer-layout" class="wrapper-medium">

		<!-- Logo -->
		<a id="footer-logo" href="<?php echo get_home_url(); ?>" title="Lien vers la page d'accueil">
			
			<?php 
			$image = get_field('logo_footer', 'option');
			if( $image ) {
				echo wp_get_attachment_image( $image );
			}?>
			<p class="leksi-green"><?php esc_html_e( 'L\'agence digitale qui pèse ses mots', 'leksi' ); ?></p>
		</a>

		<!-- Nav -->
		<nav id="footer-nav">
			<?php echo ihag_menu('footer');?>
		</nav>

		<!-- Social -->
		<div id="footer-social">
			<h2 class="h6-like"><?php esc_html_e( 'Suivez-nous', 'leksi' ); ?></h2>

			<?php 
			$link = get_field('linkedin', 'option');
			$icon = '/image/linkedin.svg';
			generate_button_social_media($link, $icon);
			?>

			<?php 
			$link = get_field('medium', 'option');
			$icon = '/image/medium.svg';
			generate_button_social_media($link, $icon);
			?>

			<?php 
			$link = get_field('facebook', 'option');
			$icon = '/image/facebook.svg';
			generate_button_social_media($link, $icon);
			?>

			<?php 
			$link = get_field('instagram', 'option');
			$icon = '/image/instagram.svg';
			generate_button_social_media($link, $icon);
			?>

			<?php 
			$link = get_field('twitter', 'option');
			$icon = '/image/twitter.svg';
			generate_button_social_media($link, $icon);
			?>

		</div>
		
		<!-- Coordonates -->
		<div id="footer-adress">
			<h2 class="h6-like"><?php esc_html_e( 'Leksi', 'leksi' ); ?></h2>
			<p class="footer-address">
				<!-- Adress -->
				<?php the_field('adress', 'option'); ?><br>
				<!-- Mail -->
				<?php $mail = get_field('mail', 'option');
				if ( $mail ) { ?>
					<a href="mailto:<?php echo $mail ?>"><?php echo $mail ?></a>
				<?php } ?>
			</p>
		</div>

		<!-- Privacy -->
		<div id="footer-privacy" class="leksi-green small-text">
			<?php echo ihag_menu('privacy');?>
		</div>
	</div>

	<p id="copyright" class="leksi-green small-text wrapper-medium">© <?php echo date("Y"); esc_html_e( ' Leksi - Tous droits reservés', 'leksi' ); ?></p>

</footer>

<?php wp_footer(); ?>

</body>
</html>
