<?php
/*
Template Name: Blog (archive)
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main> 

	<header id="archive-header" class="wrapper">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1>', '</h1>'); ?>

		<!-- Category Menu-->
		<nav class="archive-nav">

			<a href="<?php the_permalink( get_field('archive_post','option') );?>" title="<?php _e('Tous les articles','leksi');?>" class="letter-spacing discrete-link h6-like">
				<?php _e('Tous','leksi');?>
			</a>

			<?php $terms = get_terms( 
				array(
					'taxonomy' => 'category',
					'hide_empty' => true,
				) 
			);

			foreach ($terms as $row):?>
				<a href="<?php echo get_term_link($row);?>" title="<?php _e('Voir les articles ','leksi');?><?php echo $row->name;?>" class="discrete-link letter-spacing h6-like">
					<?php echo '#'. $row->name;?>
				</a>
			<?php endforeach;?>

		</nav>
		
	</header>

	<section class="wrapper archive-listing">
		
		<div class="grid-post wrapper-medium">
		<?php
			$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
			$type = "post";
			$args = array(
			    'paged' => $num_page,
			    'post_type'   => $type,
			);

			query_posts($args);
			global $wp_query; 
			if ( have_posts() ) : while (have_posts()) : the_post();
				get_template_part('template-parts/archive', "post");
			endwhile; endif;
			//wp_reset_query();
		?>

		</div>

		<nav class="pagination wrapper-medium">
			<?php joints_page_navi()?>
			<?php wp_reset_query(); ?>
		</nav>
		
	</section>

	<?php the_content('<section id="raw-content">', '</section>');?>

	<?php get_template_part( 'template-parts/newsletter' ); ?>
	<?php get_template_part( 'template-parts/contactUs' );?>

</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>