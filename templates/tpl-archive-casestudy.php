<?php
/*
Template Name: Savoir-Faire (archive)
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="page-<?php the_ID(); ?>"> 
	<header  id="archive-header" class="wrapper">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1>', '</h1>'); ?>

		<!-- Category Menu-->
		<nav class="wrapper-medium archive-nav" id="categoryCasestudy">

			<button type="button" id="resetCasestudy" class="button_casestudy link-default reset-style letter-spacing h6-like" data-slug-casestudy="0" >
				<?php _e('Tous','leksi');?>
			</button>

			<?php 
			$terms = get_terms( 
				array(
					'taxonomy' => 'casestudy_category',
					'hide_empty' => false,
				) 
			);
			foreach ($terms as $row):?>
			
				<button type="button" class="button_casestudy link-default letter-spacing reset-style h6-like" data-slug-casestudy="<?php echo $row->slug;?>" >
					<?php echo '#'. $row->name;?>
				</button>
				
			<?php endforeach;?>
		</nav>
		
	</header>

	<section class="blk-listing-casestudy wrapper archive-listing">

		<div id="listingCasestudy" class="wrapper-medium">
		
			<?php $casestudys = array(
				'posts_per_page'    => -1,
				'post_status'       => 'publish',
				'post_type'         => 'casestudy',
			);
			$postCasestudys = get_posts( $casestudys );

			foreach($postCasestudys as $post): 
				
				setup_postdata( $post );

				get_template_part( 'template-parts/archive','casestudy' );

			endforeach; 

			wp_reset_postdata();?>
		</div>

	</section>

	<article id="raw-content">
		<?php the_content(); ?>
	</article>

	<?php get_template_part( 'template-parts/contactUs' );?>

</main><!-- #page-<?php the_ID(); ?> -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>