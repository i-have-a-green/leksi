<?php
/*
Template Name: Plan du site
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="page-<?php the_ID(); ?>">

	<header id="page-header" class="wrapper padding-btm-medium <?php echo (get_field('bg_blue')) ? 'white leksi-blue-bg': '';?>">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1>', '</h1>'); ?>
		  
		<!-- Lead-paragraph -->
		<?php 
		$lead_paragraph = get_field('page_excerpt');
		if ($lead_paragraph) {
			echo '<div class="lead-paragraph entry-content">'. $lead_paragraph .'<div>';
		}
    ?>
	
	</header>

	<article id="raw-content">
    <?php the_content(); ?>
  </article>

  <!-- Site-map -->
  <nav class="wrapper">
	<div class="wrapper-medium">
		<?php echo ihag_sitemap('sitemap'); ?>
	</div>
  </nav>
	
</main><!-- #page-<?php the_ID(); ?> -->

<?php get_template_part( 'template-parts/contactUs' );?>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
