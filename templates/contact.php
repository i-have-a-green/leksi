<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="tpl-contact"><!-- Page #<?php the_ID(); ?> -->

	<header id="page-header" class="wrapper padding-btm-medium <?php echo (get_field('bg_blue')) ? 'white leksi-blue-bg': '';?>">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1>', '</h1>'); ?>
		  
		<!-- Lead-paragraph -->
		<?php 
		$lead_paragraph = get_field('page_excerpt');
		if ($lead_paragraph) {
			echo '<div class="lead-paragraph entry-content">'. $lead_paragraph .'<div>';
		}
		?>
	
	</header>

	<article id="raw-content">
		<?php the_content(); ?>
  </article>

  <section id="contact-form-container" class="wrapper v-padding-regular">

    <form name="contactForm" id="contactForm" action="#" method="POST" class="wrapper-small white-bg">

      <h2 class="lead-paragraph"><?php _e( 'Nous écrire', 'leksi' ); ?></h2>

      <input type="hidden" name="honeyPotcontact" value="">
      
      <label for="nameContact"><?php _e( 'Nom', 'leksi' ); ?> *</label>
      <input type="text" name="nameContact" id="nameContact" placeholder="<?php _e( 'Dupont', 'leksi' ); ?>" required value="">
      
      <label for="emailContact"><?php _e( 'Adresse-mail', 'leksi' ); ?> *</label>
      <input type="email" name="emailContact" id="emailContact" placeholder="<?php _e( 'nom.prenom@exemple.com', 'leksi' ); ?>" required value="">
      
      <label for="emailContact"><?php _e( 'Message', 'leksi' ); ?></label>
      <textarea type="text" name="messageContact" id="messageContact" placeholder="<?php _e( 'Rédigez votre message', 'leksi' ); ?>" rows="6" required value=""></textarea>
      
      <p class="small-text">* <?php _e( 'Champs obligatoire', 'leksi' ); ?>s</p>

      <input type="checkbox" required name="rgpdCheckbox" id="rgpdCheckbox" class="custom-checkbox">
      <label class="checkbox-label" for="rgpdCheckbox">
        <span class="checkmark"></span>
        <p><?php _e( 'J’accepte le traitement de mes données personnelles. En savoir plus sur', 'leksi' ); ?> <a href="<?php echo get_privacy_policy_url();?>"><?php _e( 'notre politque de confidentialité', 'leksi' ); ?></a>.</p>
      </label>
      
      <input class="button button-big button-blue arrow-right" type="submit" id="sendMessage" value="<?php _e( 'Envoyer', 'leksi' ); ?>">
    </form>

    <div id="validationMessage" class="center entry-content">
      <p class="h4-like"><?php the_field('contact_title');?></p>
      <p><?php the_field('contact_message');?></p>
    </div>

  </section>
	
</main><!-- #page-<?php the_ID(); ?> -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
