<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/svg" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.svg">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Skip Links-->
<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'leksi' ); ?></a>
<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'leksi' ); ?></a>
<a class="skip-link" tabindex="0"  href="#footer"><?php esc_html_e( 'Accéder au pied de page', 'leksi' ); ?></a>

<div aria-hidden="true" id="topbar-height"></div>

<nav id="topbar" class="wrapper">

	<!-- Link-home -->
	<a id="topbar-logo" href="<?php echo get_home_url(); ?>" title="<?php esc_html_e( 'Lien vers la page d\'accueil', 'leksi') ?>">
		<?php 
		$logo = get_field('logo_blue', 'options');
		$logo_alternative = get_field('logo_pink', 'options');
		if( $logo ) { 
			echo wp_get_attachment_image( $logo, "", "", array(  "id" => "logo-regular") );
		}
		if( $logo_alternative ) { 
			echo wp_get_attachment_image( $logo_alternative, "", "", array(  "id" => "logo-alternative", "class" => "-desktop-hidden -huge-hidden" ) );
		}
		?>
	</a>
	
	<div id="menu" class="letter-spacing">
		<div id="menu-buttons">
			<!-- Burger button -->
			<button id="burger-menu" class="desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'leksi' ); ?>">
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
			</button>

			<!-- Close Button -->
			<button id="close-menu" class="desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ihag' ); ?>">
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/close.svg" height="24" width="24">
			</button>
		</div>					
		
		<!-- Menu -->
		<?php echo ihag_menu('primary'); ?>

	</div>
</nav>

<!-- #content -->
<div id="content">