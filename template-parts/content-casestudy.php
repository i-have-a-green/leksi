<?php
/**
 */

?>

<main id="casestudy-content">

	<header id="page-header"  class="wrapper">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1 class="wrapper-medium h2-like">', '</h1>'); ?>
		  
		<!-- Category -->
		<?php 
		//$terms = get_the_terms($postCasestudys, 'casestudy_category-name');
		$term = ihag_get_term($post, 'casestudy_category');
		echo '<p class="category-name h6-like">#'. $term->name .'</p>';
		?>
	
	</header>

	<article id="raw-content">
		<?php the_excerpt(); ?>
		<?php the_content(); ?>
	</article>
			
</main><!-- #casestudy-<?php the_ID(); ?> -->

<aside class="wrapper center v-padding-medium">
	<p class="h2-like"><?php esc_html_e( ' Toutes les études de cas', 'leksi' ); ?></p>
	<a class="button-ghost arrow-left button-big" href="<?php the_permalink(get_field('archive_casestudy', 'option')); ?>">Revenir au menu</a>
</aside>

<?php get_template_part( 'template-parts/contactUs' );?>
