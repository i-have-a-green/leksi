<?php
/**
 *
 */
?>

<main id="post-<?php the_ID(); ?>">

	<header id="post-header">

		<!-- Fil d'Ariane -->
		<div id="post-breadcrumb" class="wrapper"><?php wpBreadcrumb() ?></div>
		
		<!-- Thumbnail -->
		<div id="post-thumb">
			<?php 
			$size = 'couverture_article';
			if ( has_post_thumbnail() ) {
				the_post_thumbnail($size);
			} else {
				$image = get_field('image_fallback', 'option');
				if( $image ) {
					echo wp_get_attachment_image( $image, $size );
				}
			} ?>
		</div>

		<div id="post-title" class="wrapper v-padding-small">
			<!-- Titre-->
			<?php the_title('<h1>', '</h1>'); ?>

			<!-- Category -->
			<?php $terms= get_the_terms($post, 'category');?>
			<a class="category letter-spacing discrete-link" href="<?php echo get_term_link($terms[0]); ?>"><?php echo '#'. $terms[0]->name; ?></a><br>
			
			<!-- Publication -->
			<div class="text-like wrapper-medium">
				<?php
				$time_string = 'Article publié le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
					echo sprintf( $time_string,
					esc_attr( get_the_date( DATE_W3C ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( DATE_W3C ) ),
					esc_html( get_the_modified_date() )
				);
				?>
			</div>
		</div>

	</header>

	<article id="post-layout" class="v-padding-small">

		<aside id="post-share">
			<div class="author-bloc">
				<div class="author-avatar">
					<?php echo get_avatar( get_the_author_id(), 48); ?>
				</div>
				<div class="author-text">
					<p><?php esc_html_e( 'Rédigé par', 'leksi' ); ?><br></p>
					<h3 class="h6-like"><?php echo get_the_author();?></h3>
				</div>
			</div><!-- Author-bloc -->  
			<div id="share-list">
				<h2 class="text-like"><?php esc_html_e( 'Partager', 'leksi' ); ?></h2>
				<div id="share-links">
					<a class="JSrslink link-icon-variant" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/share-twitter.svg" height="24" width="24">
					</a>
					<a class="JSrslink link-icon-variant" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/share-linkedin.svg" height="24" width="24">
					</a>
					<a class="JSrslink link-icon-variant" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/share-facebook.svg" height="24" width="24">
					</a>
					<div class="copyLink link-icon-variant"> 
						<input type="text" class="copyLinkValue" value="<?php echo get_the_permalink();?>">
						<img alt="<?php esc_html_e('Copier le lien', 'leksi')?>" src="<?php echo get_template_directory_uri(); ?>/image/share-url.svg" height="24" width="24">
						<div class="copyLinkMessage active">
							<?php esc_html_e('Lien copié dans le presse-papier !', 'sparknews')?>
						</div>
					</div>
				</div>
			</div>
		</aside>

		<div id="raw-content" class="post-width-limit">
			<?php the_content();?>
		</div>

	</article>


	<?php 
	$cat = array(
		'posts_per_page' => 2,
		'post_status' => 'publish',
		'exclude' => array($post->ID),
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => array( $terms[0]->slug)
			)
		)
	);

	$posts = get_posts( $cat );

	if(sizeof($posts) > 0):
	?>

	<section id="post-suggestions" class="wrapper archive-listing v-padding-regular leksi-blue-bg">
		
		<h2 class="leksi-green center"><?php esc_html_e( ' Dans la même catégorie', 'leksi' ); ?></h2>
		<div class="grid-post wrapper-medium">

			<?php 
			foreach($posts as $post): 
			setup_postdata( $post );
			get_template_part( 'template-parts/archive','post' );
			endforeach; 
			wp_reset_postdata();?>

		</div>
	
	</section>

	<?php endif;?>


	<?php // if(get_field('is_podcast', $terms[0])):?><?php // endif;?>

	<?php if ( get_field('podcast_enabled', 'option') ) { ?>

	<section id="blk-banner-podcast" class="center v-padding-medium">
		<h2 class="center">
			<?php the_field('podcast_title_start', 'option') ?>
			<?php if ( get_field('podcast_color', 'option') ) {
				echo '<span class="white">'. get_field('podcast_title_white', 'option') .'</span>';
				the_field('podcast_title_end', 'option');
			} ?>	
		</h2><br>
		<a class="buttton-ghost button-big arrow-right" href="<?php echo get_term_link( $terms[0], 'category'); ?>"><?php esc_html_e( 'Voir les podcasts', 'leksi' ); ?></a>
	</section>

	<?php } ?>		

</main><!-- #post-<?php the_ID(); ?> -->
