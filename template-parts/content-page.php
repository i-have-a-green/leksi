<?php
/**
 *
 */
?>

<main id="page-<?php the_ID(); ?>">

	<header id="page-header" class="wrapper <?php echo (get_field('bg_blue')) ? 'white leksi-blue-bg': '';?>">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1>', '</h1>'); ?>
		  
		<!-- Lead-paragraph -->
		<?php 
		$lead_paragraph = get_field('page_excerpt');
		if ($lead_paragraph) {
			echo '<div class="lead-paragraph entry-content">'. $lead_paragraph .'<div>';
		}
		?>
	
	</header>

	<article id="raw-content">
		<?php the_content(); ?>
	</article>
	
</main><!-- #page-<?php the_ID(); ?> -->

<?php get_template_part( 'template-parts/contactUs' );?>

