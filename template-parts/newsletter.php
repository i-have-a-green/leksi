<?php if(get_field("authToken", 'option')):?>

<section id="blk-newsletter" class="wrapper leksi-green">

    <?php $is_underlined = get_field('newsletter_is_underlined', 'option');?>

    <h2 class="center <?php if ($is_underlined) { echo 'line-underlined';} ?>">
        <?php the_field('newsletter_title', 'option');?>

        <?php if ($is_underlined) { 
            echo '<span class="font-title">'; 
            the_field('newsletter_titre_custom', 'option');
            echo '</span>';
            } 
        ?>
    </h2>

    <form id="footer-newslettter-form" class="wrapper-small">
        <input type="hidden" name="honeyPotWhitePaper" value="">

        <div class="layout">
            <label for="newletter_email"><?php _e("Adresse-mail", "leksi");?></label>
            <input type="email" class="margin-b" name="newletter_email" id="newletter_email" placeholder="<?php _e("nom@mail.com", "leksi");?>" required>
            <input class="button button-green arrow-right" type="submit" id="sendMessageNewsletter" value="<?php _e("S'inscrire", "leksi");?>">
        </div>


        <input type="checkbox" required name="rgpdCheckbox" id="rgpdCheckbox" class="custom-checkbox">
        <label class="checkbox-label" for="rgpdCheckbox">
            <span class="checkmark"></span>
            <p><?php _e( "J'accepte de recevoir la newsletter de l'agence Leksi. En savoir plus sur", "leksi" ); ?> <a href="<?php echo get_privacy_policy_url();?>"><?php _e( 'notre politque de confidentialité', 'leksi' ); ?></a>.</p>
        </label>
        
    </form>

    <div id="newsletterMessage" class="entry-content center">
        <p class="h4-like"><?php the_field('newsletter_message_title', 'option');?></p>
        <p><?php the_field('newsletter_message_content', 'option');?></p>
    </div>

</section>

<?php endif;?>