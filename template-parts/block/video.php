<?php
/**
 * Block Name: Bloc Video
 */
 ?>

<?php $block_uniq_id = "id_".uniqid(); ?>


<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-video wrapper v-padding-small">

    <div class="embed-container">

        <?php
        $video = get_field('link');
        if ( !$video ) :?>
            <em>Renseigner le bloc</em>
            
        <?php else :?>

              	<div class="embed-container">

                  <?php 
                 // Test if thumbnail_video doesn't exists
                 if (!get_field('thumbnail_video')) {
                 ?>
                 <a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                    <script> 
                        if (typeof iframe === 'undefined') {
                         var iframe = new Object();
                        }
                        iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link'); ?>'; 
                    </script>
 
                     <?php
                         $video = get_field('link');
                         $id = retrieve_id_video($video);
                     ?>
 
                     <img src="http://img.youtube.com/vi/<?php echo $id; ?>/mqdefault.jpg">
                 </a> 
                 
             <?php } else { ?>
 
                     <a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                     <script> 
                        if (typeof iframe === 'undefined') {
                         var iframe = new Object();
                        }
                        iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link'); ?>'; 
                    </script>

                     <?php
                     $videoThumbnail = get_field('thumbnail_video');
                     $size = 'thumbnail-video';
                     echo wp_get_attachment_image($videoThumbnail, $size);?>
                     </a>
             <?php } ?>
 
         </div>

        <?php endif; ?>

    </div>
</section>


