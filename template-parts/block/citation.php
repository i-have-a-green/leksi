<?php
/**
 * Block Name: Bloc Citation
 */
 ?>

<?php
// si quote renseigné
$quote = get_field('quote');
if ( !$quote ) :?>
    <em>Renseigner le bloc</em>
    
<?php else :?>

	<blockquote class="wrapper-small entry-content">

		<h2 class="baseline-paragraph"><?php the_field('quote');?></h2>
		<?php 
		$author = get_field('author_reference');
		$commment = get_field('additional_comment');
		// si author_reference renseigné
		if ( $author) {
			echo '<cite class="h6-like">'. $author .'</cite>';
		} 
		// si additional_comment renseigné
		if ( $comment ) {
			echo '<p class="small-text">'. $comment .'</p>';
		} 
		?>

	</blockquote>
		
<?php endif; ?>
