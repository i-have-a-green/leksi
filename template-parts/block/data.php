<?php
/**
 * Block Name: Bloc Data
 */
 ?>
 <section class="blk-data wrapper v-padding-regular">

<?php
$description = get_field('description');
if ( !$description ) :?>
    <em>Renseigner le bloc</em>
    
<?php else :?>

  <div class="grid-title wrapper-medium">
  
    <?php $is_underlined = get_field('title_is_underlined'); ?>

    <h2 class="bloc-title <?php if ($is_underlined) { echo 'title-underlined';} ?>">
			<?php the_field('title');?>

			<?php if ($is_underlined) { 
				echo '<span>'; 
				the_field('title_underlined');
				echo '</span>';
				} 
			?>
		</h2>

    <div class="data-text">
      <div class="entry-content"><?php the_field('description');?></div>
      <?php 
      $link = get_field('link');
      if ( $link ) : 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
      <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
      <?php endif; ?>
    </div>

  </div>

  <div class="data-loop wrapper-medium">
    <?php if( have_rows('items') ):
        while( have_rows('items') ) : the_row();
        
          $image = get_sub_field('image');
          //$size='blk-data';
          ?>

          <article class="data-card">

            <?php if ( $image ): ?>
              <div class="data-img-container">
                <?php echo wp_get_attachment_image($image, $size, "", array ("class" => "data-img")); ?>
              </div>
            <?php endif; ?>

            <div class="entry-content"><?php the_sub_field('description'); ?></div>

          </article>

          <?php

        endwhile;
    
    endif; ?>
  </div>
      
  <?php endif; ?>

</section>



