<?php
/**
 * Block Name: Bloc Podcast
 */
 ?>

<section class="blk-podcast wrapper v-padding-small">

<?php
$title = get_field('title');
if ( !$title ) :?>

    <em>Renseigner le bloc</em>
    
<?php else :?>

	<div class="wrapper-small">

		<div class="podcast-header leksi-blue-bg white">
			<?php 
			// 1 - Podcast Header
			// si image renseignée :
			$image = get_field('image');
			$size = 'podcast'; 
			if( $image ) {
				echo '<div class="podcast-thumbnail">';
				echo wp_get_attachment_image( $image, $size );
				echo '</div>';
			}
			?>

			<div class="podcast-title">
				<!-- si title renseigné -->
				<h2 class="baseline-paragraph"><?php the_field('title');?></h2>

				<?php 
				// si durée renseignée
				if( get_field('duration') ) {
					echo '<p>'.get_field('duration').'</p>';
				}
				?>

			</div>
		</div>

		<?php 
		// 2 - Audio - si fichier audio renseigné :
		$audio_file = get_field('audio_file');
		if( $audio_file ) {
			echo '<div class="podcast-audio">';
			echo wp_audio_shortcode( array( 'src' => $audio_file['url']) );
			echo '</div>';
		}
		?>

		<nav>

			<?php 
			$spotify = get_field('link_spotify');
			$apple = get_field('link_apple_podcast');
			$google = get_field('link_google_podcast');
			?>

			<?php if($spotify) { ?>	
				<a class="link-icon" href="<?php echo $spotify ?>" target="_blank" title="<?php _e( 'Écouter sur Spotify', 'leksi' ); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/spotify.svg" aria-hidden="true">
					<p><?php _e( 'Spotify', 'leksi' ); ?></p>
				</a>
			<?php } ?>

			<?php if($apple) { ?>	
				<a class="link-icon" href="<?php echo $apple ?>" target="_blank" title="<?php _e( 'Écouter sur Apple Podcast', 'leksi' ); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/applepodcast.svg" aria-hidden="true">
					<p><?php _e( 'Apple Podcast', 'leksi' ); ?></p>
				</a>
			<?php } ?>

			<?php if($google) { ?>	
				<a class="link-icon" href="<?php echo $google ?>" target="_blank" title="<?php _e( 'Écouter sur Google Podcast', 'leksi' ); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/googlepodcast.svg" aria-hidden="true">
					<p><?php _e( 'Google Podcast', 'leksi' ); ?></p>
				</a>
			<?php } ?>

		</nav>

	</div> <!-- /.wrapper-small-->

<?php endif;?>

</section>
