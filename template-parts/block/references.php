<?php
/**
 * Block Name: Bloc References
 */
 ?>
 <section class="blk-reference wrapper v-padding-regular">

 <?php
$title = get_field('title');
if ( !$title ) :?>

	<em>Renseigner le bloc</em>
	
<?php else :?>

	<h2 class="wrapper-medium"><?php the_field('title');?></h2>

	<?php if( have_rows('logos') ): ?>

		<div class="reference-container wrapper-medium">

			<?php while( have_rows('logos') ) : the_row();?>

				<div>

					<?php 
					$item_title = get_sub_field('title');
					$image = get_sub_field('logo'); 
					$size = '140';
					$link = get_sub_field('link');

					// Si logo + lien
					if( (!empty($image)) && (!empty($link))){
						?>
						<a href="<?php echo esc_url( $link ); ?>" target="_blank">
							<?php echo wp_get_attachment_image( $image, $size );?>
							<p class="appear-on-hover"><?php echo $item_title ?></p>
						</a> 
						<?php 
					}

					// Si logo, mais pas de lien
					else if( (!empty($image ))) {
						echo wp_get_attachment_image( $image, $size );
						echo '<p class="appear-on-hover">'. $item_title .'</p>';
					}

					// Si pas de logo 
					else {
						echo '<p>'. $item_title .'</p>';
					}
					?>

				</div>

			<?php endwhile; ?>

		</div><!-- /refeerence-container -->

	<?php endif; ?>
	

<?php endif; ?>

</section>
