<?php
/**
 * Block Name: Bloc Banniere
 */
 ?>

<section class="blk-banner wrapper">

<?php
$link = get_field('link');
if ( !$link ) :?>

    <em>Renseigner le bloc</em>
    
<?php else :?>

  <h2 class="line-underlined">
    <?php 
    the_field('title');
    if ( get_field('title_variant') ) {
      echo '<span>'. get_field('title_variant') .'</span>';
    }
    ?>
  </h2>

	<!-- si lien renseigné -->
	<?php 
    if ( $link ) : 
      $link_url = $link['url'];
      $link_title = $link['title'];
      $link_target = $link['target'] ? $link['target'] : '_self';
      ?>
    <a class="button-big arrow-right" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
	<?php endif; ?>

<?php endif; ?>

</section>
