<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<?php

$image = get_field('image');

if ( empty($image) ):?>

	<em>Renseigner l'image</em>
	
<?php else :?>

	<div class="wrapper wrapper-medium center v-padding-small">
	
		<?php 
		// Image
		$size = 'wrapper';
		echo wp_get_attachment_image($image, $size);
		// Caption
		if ( wp_get_attachment_caption($image) ) {
			echo '<p class="small-text entry-content">'. wp_get_attachment_caption($image) .'</p>'; 
		}
		?>

	</div>

<?php endif; ?>

