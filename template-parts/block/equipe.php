<?php
/**
 * Block Name: Bloc Equipe
 */
 ?>
 <section class="blk-team wrapper v-padding-regular leksi-blue-bg leksi-green">
<?php
$title = get_field('title');
if ( !$title ) :?>

	<em>Renseigner le bloc</em>
	
<?php else :?>

	<h2 class="wrapper-medium center"><?php the_field('title');?></h2>

	<div class="wrapper-medium grid-regular">

    <?php if( have_rows('portraits') ):
		while( have_rows('portraits') ) : the_row();?>
		
			<article class="portrait-card">

				<header class="portrait-header">
					<?php 
					// si photo renseignée
					$image = get_sub_field('photo');
					$size = '100-100'; 
					if( !$image ) {
						$image = get_field('image_fallback', 'option');
					}
					echo '<div class="portrait-picture">';
					echo wp_get_attachment_image( $image, $size );
					echo '</div>';
					?>

					<div class="portrait-info">
						<?php 
						$name = get_sub_field('name');
						$job = get_sub_field('job');
						$linkedin = get_sub_field('linkedin_link');

						if ($name) {
							echo '<h3 class="h4-like">'. $name .'</h3>';
						}

						if ($job) {
							echo '<h4 class="text-like">'. $job .'</h4>';
						}

						if ($linkedin) {
							$icon = '/image/linkedin-green.svg';
							generate_button_social_media($linkedin, $icon);
						}
						?>
					</div>
				</header>
				
				<div class="portrait-content entry-content">
					<?php the_sub_field('biography');?>
				</div>

			</article>

		<?php endwhile; ?>
		
	<?php endif; ?>

	</div>
	
<?php endif;?>

</section>
