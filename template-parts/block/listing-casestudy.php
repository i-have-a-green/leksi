<?php
/**
 * Block Name: Bloc Listing casestudy
 */
 ?>
 <section class="blk-listing-casestudy v-padding-regular wrapper">

<?php
$description = get_field('description');
if ( !$description ) :?>
    <em>Renseigner le bloc</em>
    
<?php else :?>

	<div class="grid-regular wrapper-medium">
		<h2 class="small-title">
			<?php the_field('title');?>
			<?php 
			$title_custom = get_field('title_custom');
			if ($title_custom) {
				echo '<span class="font-title">'.$title_custom.'</span>';
			}
			?>
		</h2>

		<div class="entry-content">
			<div class="lead-paragraph"><?php the_field('description');?></div>
			<a class="margin-top read-more-blue" href="<?php the_permalink(get_field('archive_casestudy','option'));?>">
				<?php _e( '> En savoir plus', 'leksi' ); ?>
			</a>
		</div>
	</div><!-- /grid-title -->

	<?php if( have_rows('casestudys_list') ): ?>
	
	<div class="wrapper-medium">

		<?php while( have_rows('casestudys_list') ) : the_row();
		
			$tax_casestudy = get_sub_field("tax_casestudy");

			echo '<article class="card-casestudy grid-regular">';

				// Casestudy Title 
				echo '<h3 class="title-underlined"><span>'.$tax_casestudy[0]->name.'</span></h3>';

				//Casestudy Content
				echo '<div class="entry-content">';
					echo '<p>'. get_the_excerpt(get_sub_field('select_casestudy')). '</p>';

					echo '<a class="margin-top read-more-blue" href="'.get_permalink(get_sub_field('select_casestudy')).'">';
						_e( '> En savoir plus', 'leksi' );
					echo'</a>';
				echo'</div>';

			echo '</article>';

		endwhile; ?>

		<?php endif; ?>

		<a class="button-ghost" href="<?php the_permalink(get_field('archive_casestudy','option'));?>#listingCasestudy"><?php esc_html_e( 'Voir les études de cas', 'leksi' ); ?></a>

	</div><!-- /lsiting-grid -->

<?php endif; ?>

</section>
