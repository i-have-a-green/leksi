<?php
/**
 * Block Name: Bloc Témoignage
 */
 ?>
 <section class="blk-testimonial wrapper v-padding-regular">

 <?php
$title = get_field('title');
$is_underlined = get_field('title_is_underlined');
if ( !$title ) :?>
	<em>Renseigner le bloc</em>
	
<?php else :?>

	<div class="grid-regular wrapper-medium">

		<h2 class="leksi-green bloc-title <?php if ($is_underlined) { echo 'title-underlined';} ?>">
			<?php the_field('title');?>

			<?php if ($is_underlined) { 
				echo '<span>'; 
				the_field('title_underlined');
				echo '</span>';
				} 
			?>
		</h2>

		<?php if( have_rows('testimonial') ): ?>

		<div class="testimonial-container leksi-green">	

			<?php while( have_rows('testimonial') ) : the_row();?>

			<div class="entry-content">

				<?php

				// Testimonial
				$content = get_sub_field('testimonial');
				$name = get_sub_field('name');
				$job = get_sub_field('job');

				if ($content) {
					echo '<div class="baseline-paragraph">'. $content .'</div>';
				}

				// Name
				if ($name) {
					echo '<h3 class="h6-like">'. $name.'</h3>';
				}
				
				// Job
				if ($job) {
					echo '<p>'. $job .'</p>';
				}
				?>

			</div>

			<?php endwhile; ?>

		</div>

		<?php endif;?>
		
	</div>

<?php endif;?>

</section>
