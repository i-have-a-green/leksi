<?php
/**
 * Block Name: Bloc TitleTexte
 */
 ?>

<?php
$description = get_field('description');
if ( !$description ) :?>

    <em>Renseigner le bloc</em>
    
<?php else :?>

<?php $is_underlined = get_field('title_is_underlined');?>

	<section class="blk-text wrapper <?php echo ( get_field('decor') ) ? 'custom-bg' : '';?>"><!-- class "custom-bg" pour element graphique -->

		<div class="wrapper-medium">

			<div class="bloc-title">
				<h2 class="<?php if ($is_underlined) { echo 'title-underlined';} ?>">
					<?php the_field('title');?>

					<?php if ($is_underlined) { 
						echo '<span>'; 
						the_field('title_variant');
						echo '</span>';
						} 
					?>
				</h2>
			</div>

			<div class="bloc-content">

				<div class="entry-content"><?php the_field('description');?></div>
				
				<?php
				// Link
				$link = get_field('link');

				if ($link) {

					$link_url = $link['url'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					$link_title = $link['title'];

					// Link Title Fallback
					if ( !$link_title ) {
						$link_title = '> En savoir plus';
					}
					?>

					<a class="button arrow-right" href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo esc_attr( $link_target ); ?>" ><?php echo  esc_html( $link_title); ?></a>

					<?php
				}
				?>
			
			</div>

		</div>

	</section>
	
<?php endif; ?>
