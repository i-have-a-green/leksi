<?php
/**
 * Block Name: Bloc TitleTexteImage
 */
 ?>

<?php
$description = get_field('description');
$image = get_field('image');
$is_underlined = get_field('title_is_underlined');

if ( empty($image) ):?>

    <em>Renseigner le bloc Titre + Texte  + Image</em>
    
<?php else :?>

	<section class="blk-text wrapper <?php echo ( get_field('decor') ) ? 'custom-bg' : '';?>"><!-- class "custom-bg" pour element graphique -->

		<div class="wrapper-medium">

			<div class="bloc-title">

				<h2 class=" <?php if ($is_underlined) { echo 'title-underlined';} ?>">
					<?php the_field('title');?>

					<?php if ($is_underlined) { 
						echo '<span>'; 
						the_field('title_variant');
						echo '</span>';
						} 
					?>
				</h2>

				<div class="entry-content"><?php the_field('description');?></div>

				<?php // Link
				$link = get_field('link');

				if ($link) {

					$link_url = $link['url'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					$link_title = $link['title'];

					// Link Title Fallback
					if ( !$link_title ) {
						$link_title = '> En savoir plus';
					}
					?>

					<a class="button arrow-right" href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" target="<?php echo esc_attr( $link_target ); ?>" ><?php echo  esc_html( $link_title); ?></a>

					<?php
				}
				?>
			</div>

			<div class="bloc-content">
				<?php 
				// le test 'if ($image)' a deja ete fait au dessus l11 
				$size = 'bloc-text-image';
				echo wp_get_attachment_image($image, $size);
				// Caption
				if ( wp_get_attachment_caption($image) ) {
					echo '<p class="small-text entry-content">'. wp_get_attachment_caption($image) .'</p>'; 
				}
				?>
			</div>

		</div>

	</section>
	
<?php endif; ?>

