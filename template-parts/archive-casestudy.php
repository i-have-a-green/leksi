<?php 
$category = ihag_get_term($postCasestudys, 'casestudy_category');
?>

<?php
echo '<article class="card-casestudy archiveCasestudy grid-regular" data-slug-casestudy="'. $category->slug .'">';

    // Casestudy Title 
    echo '<div>';
        echo '<h3 class="title-underlined"><span>'. get_the_title() .'</span></h3>';
        echo '<p class="h6-like letter-spacing">#'. $category->name .'</p>';  
    echo '</div>';

    //Casestudy Content
    echo '<div class="entry-content">';
        echo '<p>'. get_the_excerpt() .'</p>';
        echo '<a class="margin-top read-more-blue" href="'.get_permalink().'">';
            _e( '> En savoir plus', 'leksi' );
        echo'</a>';
    echo'</div>';

echo '</article>';
?>