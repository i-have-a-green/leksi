<?php

// Template part to redirect to contact.php
$contact_show = get_field('contact_show', 'options');
$contact_content = get_field('contact_content', 'options');
$contact_link = get_field('contact_link', 'options');

if ($contact_show) { ?>

<aside id="blk-contact" class="center wrapper">
    <h2><?php echo $contact_content; ?></h2><br>
    <a class="button-big arrow-right" href="<?php echo $contact_link['url']; ?>" title="<?php echo $contact_link['title']; ?>"><?php echo $contact_link['title']; ?></a>
</aside>

<?php }?>