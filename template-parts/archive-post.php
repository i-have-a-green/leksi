<?php 
$tax_cat = ihag_get_term($post, 'category');
?>

<a href="<?php the_permalink(); ?>" title="<?php the_title() ?>" class="post-card bg-hover">

    <!-- Pseudo element after pour afficher image play de Podcast -->
    <div class="card-thumbnail <?php echo (get_field("is_podcast", $tax_cat)) ? 'is_podcast' : '';?> " href="<?php the_permalink(); ?>" title="<?php the_title() ?>">
        <?php 
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('post-thumb-archive');
        } else {
            $image = get_field('image_fallback', 'option');
            $size = 'post-thumb-archive';
            if( $image ) {
                echo wp_get_attachment_image( $image, $size );
            }
        } ?>
    </div>

    <?php 
    //echo '<a class="category-name h6-like letter-spacing discrete-link" href="'. get_term_link( $tax_cat ) .'" title="'; _e( 'Voir la catégorie ', 'leksi' ); echo $tax_cat->name .'">';
    //echo '#'.$tax_cat->name;
    //echo '</a>';

    echo '<p class="category-name">#'. $tax_cat->name .'</p>';
    ?>

    <h2 class="card-title h5-like"><?php the_title();?></h2>

    <time class="text-like"><?php echo get_the_date();?></time>

    <p class="arrow-right read-more" href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php _e( 'Lire l\'article', 'leksi' ); ?></p>

</a><!-- ./post-card -->