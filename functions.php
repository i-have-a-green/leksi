<?php

include_once("inc/acf.php");
include_once("inc/acf-block.php");
include_once("inc/settings-gutenberg.php");
include_once("inc/widget.php");
include_once("inc/clean.php");
include_once("inc/no-comment.php");
include_once("inc/images.php");
include_once("inc/custom-post-type.php");
include_once("inc/breadcrumb.php");
include_once("inc/menu.php");
include_once("inc/enqueue_scripts.php");
include_once("inc/contact.php");
include_once("inc/modale.php");
include_once("inc/pagination.php");


// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );

function ihag_unregister_taxonomy(){
    register_taxonomy('post_tag', array());
    //register_taxonomy('category', array());
	//add_post_type_support( 'page', 'excerpt' );
	//remove_post_type_support( 'page', 'thumbnail' );
}
add_action('init', 'ihag_unregister_taxonomy');

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
    if ( isset( $_GET['from_post'] ) ) {
        $my_post = get_post( $_GET['from_post'] );
        if ( $my_post )
            return $my_post->post_content;
    }
    return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts($query) {

  if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
      $query->set('post_type', array("post", "rendez-vous", "solution"));
      $query->set('posts_per_page', -1);
  } 

}
//add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL($url)  {
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

function change_show_in_nav_menus_post( ) {
  register_post_type('post', array(
      'show_in_nav_menus' => true
    )
  );
}
//add_filter( 'init', 'change_show_in_nav_menus_post', 10, 2 );

function generate_button_social_media($link, $icon){
  if( $link ): 
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a class="link-icon " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
      <?php if ($icon) { ?>
        <img src="<?php echo get_stylesheet_directory_uri(); echo $icon ?>" aria-hidden="true" alt="$link_title">
      <?php } ?>
      <p><?php echo esc_html( $link_title ); ?></p>
    </a>
  <?php endif; 
};


/*recupère l'url d'une video */
function retrieve_id_video($video){

  preg_match('/src="(.+?)"/', $video, $matches_url );
  $src = $matches_url[1];	

  preg_match('/embed(.*?)?feature/', $src, $matches_id );
  $id = $matches_id[1];
  $id = str_replace( str_split( '?/' ), '', $id );

  return $id;
}

//Si plusieurs taxonomies renseignées -> afficher que la premiere (sans supp les autres)
function ihag_get_term($post, $taxonomy){
  
  if ( class_exists('WPSEO_Primary_Term') ):
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, get_the_id( $post ) );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
  $term = get_term( $wpseo_primary_term );
  if ( is_wp_error( $term ) ) {
    $term = get_the_terms($post, $taxonomy);
    return $term[0];
  }
  return $term;
 else:
 	$term = get_the_terms($post, $taxonomy);
	return $term[0];
 endif;
}
