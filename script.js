var buttons_casestudy = document.getElementsByClassName('button_casestudy');
Array.prototype.forEach.call(buttons_casestudy, function(button_casestudy) {
    button_casestudy.addEventListener('click', function(e) {

        var archiveCasestudy = document.getElementsByClassName('archiveCasestudy');
        Array.prototype.forEach.call(archiveCasestudy, function(casestudy) {

            var classeCasestudy = casestudy.classList;
            //console.log((button_casestudy.dataset.slugCasestudy + " == " + casestudy.dataset.slugCasestudy));
            if (button_casestudy.dataset.slugCasestudy == casestudy.dataset.slugCasestudy || button_casestudy.dataset.slugCasestudy == "0") {
                classeCasestudy.remove("hide");
            } else {
                classeCasestudy.add("hide");
            }

        });
    });
});
if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('contact-form-container').classList.add("message-sent");
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("footer-newslettter-form")) {
    document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
        document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("footer-newslettter-form");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl + 'formNewsletter', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageNewsletter').disabled = false;
                document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
                document.getElementById('blk-newsletter').classList.add("form-sent");
            }
        };
        xhr.send(formData);
    });
}

/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection
------------------
// 4 - Modif topbar background on scroll

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("topbar").classList.toggle("menu-open");
}

// 3 - Scroll detection
// #topbar gets sticky on scroll

var derniere_position_de_scroll_connue = 0;
var ticking = false;
var menu = document.getElementById("topbar");
var sticky = menu.offsetTop;

window.addEventListener('scroll', function(e) {
    if ( menu.classList.contains("menu-open") ) {
        // nothing happens
        //console.log('Menu Ouvert - pas de scroll');
    } else {
        if (!ticking) {
            //console.log('Détection du scroll');
            window.setTimeout(function() {
                ticking = false;
                if (window.pageYOffset > sticky) {
                    menu.classList.add("sticky");
                } else {
                    menu.classList.remove("sticky");
                }
            }, 50); //fréquence du scroll
        }
        ticking = true;
    }
});


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/


document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
// organise - infinit scroll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}
document.addEventListener('DOMContentLoaded', function () {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) { 
            e.stopPropagation();
            e.preventDefault();
            var width  = 320, 
            height = 400,
            left   = (widthScreen - width)  / 2, 
            top    = (heightScreen - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                             ',width='  + width  +
                             ',height=' + height +
                             ',top='    + top    +
                             ',left='   + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });
    
    copyLink = document.getElementsByClassName('copyLink');
    Array.prototype.forEach.call(copyLink, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            copyLinkValue = el.getElementsByClassName('copyLinkValue');
            copyLinkValue[0].focus();
            copyLinkValue[0].select();
            document.execCommand("copy");
            el.classList.add('active');
            // Message disappears after 4.25s
            setTimeout(function(){ el.classList.remove('active'); }, 4250);
        });

    });

});
