<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'leksi',
                'title' => __( 'Leksi', 'ihag' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
       
        acf_register_block_type(
            array(
                'name'				    => 'title-texte',
                'title'				    => __('Titre + texte'),
                'description'		    => __('Titre, texte, bouton (en option)'),
                'placeholder'		    => __('TitleTexte'),
                'render_template'	    => 'template-parts/block/titleTexte.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('agence', 'titre', 'texte'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'title-texte-image',
                'title'				    => __('Titre + texte + image'),
                'description'		    => __('Image, titre, texte, bouton (en option)'),
                'placeholder'		    => __('TitleTexteImage'),
                'render_template'	    => 'template-parts/block/titleTexteImage.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'media-document',
                'keywords'			    => array('agence', 'titre', 'texte', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'image',
                'title'				    => __('Image'),
                'description'		    => __('Image + légende (en option)'),
                'placeholder'		    => __('image'),
                'render_template'	    => 'template-parts/block/image.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'format-image',
                'keywords'			    => array('agence', 'bloc', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        
        acf_register_block_type(
            array(
                'name'				    => 'video',
                'title'				    => __('Vidéo'),
                'description'		    => __('Bloc vidéo compatible avec Youtube et Dailymotion.'),
                'placeholder'		    => __('video'),
                'render_template'	    => 'template-parts/block/video.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'video-alt3',
                'keywords'			    => array('agence', 'bloc', 'video'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'podcast',
                'title'				    => __('Podcast'),
                'description'		    => __('Fichier audio + liens vers les plateformes de streaming (Spotify, Apple Podcast, Google Podcast) '),
                'placeholder'		    => __('podcast'),
                'render_template'	    => 'template-parts/block/podcast.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'format-audio',
                'keywords'			    => array('agence', 'bloc', 'podcast'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'citation',
                'title'				    => __('Citation'),
                'description'		    => __('Bloc Citation'),
                'placeholder'		    => __('citation'),
                'render_template'	    => 'template-parts/block/citation.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'editor-quote',
                'keywords'			    => array('agence', 'bloc', 'citation'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'equipe',
                'title'				    => __('Équipe'),
                'description'		    => __('Portraits de l\'équipe Leksi'),
                'placeholder'		    => __('equipe'),
                'render_template'	    => 'template-parts/block/equipe.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'admin-users',
                'keywords'			    => array('agence', 'bloc', 'equipe'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'temoignage',
                'title'				    => __('Témoignage'),
                'description'		    => __('Témoignage client'),
                'placeholder'		    => __('temoignage'),
                'render_template'	    => 'template-parts/block/temoignage.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'testimonial',
                'keywords'			    => array('agence', 'bloc', 'temoignage'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'references',
                'title'				    => __('Logos'),
                'description'		    => __('Listing de logos'),
                'placeholder'		    => __('Logos'),
                'render_template'	    => 'template-parts/block/references.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'images-alt2',
                'keywords'			    => array('agence', 'bloc', 'references'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'data',
                'title'				    => __('Data'),
                'description'		    => __('Liste de datas sur le numérique responsable (accueil))'),
                'placeholder'		    => __('data'),
                'render_template'	    => 'template-parts/block/data.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'chart-pie',
                'keywords'			    => array('agence', 'bloc', 'data'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'banniere',
                'title'				    => __('Bannière "On change ?"'),
                'description'		    => __('Lien vers la page Contact'),
                'placeholder'		    => __('banniere'),
                'render_template'	    => 'template-parts/block/banniere.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'admin-links',
                'keywords'			    => array('agence', 'bloc', 'banniere'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'listing-casestudy',
                'title'				    => __('Études de cas (accueil)'),
                'description'		    => __('Liste de 3 études de cas (page d\'accueil).'),
                'placeholder'		    => __('Études de cas (accueil)'),
                'render_template'	    => 'template-parts/block/listing-casestudy.php',
                'category'			    => 'leksi',
                'mode'                  => 'edit',
                'icon'				    => 'awards',
                'keywords'			    => array('agence', 'bloc', 'listing', 'casestudy'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

    }
}

